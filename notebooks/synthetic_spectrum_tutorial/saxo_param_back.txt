# Background fitted parameters 
# Parameters are given in the following order:
# ['A_H_1' 'nuc_H_1' 'alpha_H_1' 'A_H_2' 'nuc_H_2' 'alpha_H_2' 'a' 'b'
#  'A_Gauss' 'numax' 'Wenv' 'noise']
# Peakbagging performed by SNB with apollinaire (v0.13) 
# https://apollinaire.readthedocs.io/en/latest 
# File creation date: 21/06/2021 12:03:32
# Column 0: median of the fitted distribution. Column 1: parameter standard deviation 
# ######################
# Settings:
# apodisation=False
# low_cut=50.0
# nwalkers=500
# nsteps=500
# discarded_steps=100
# quickfit=True
# reboxing_behaviour=advanced_reboxing
# #######################
# 
1.999014496118326 0.34205632271493935
532.6569212500478 113.32401728645277
2.594426885401207 0.93911289516234
0.168589378958593 0.15719783834916173
2095.416026492858 1342.8133422114138
4.306171315928544 1.096366292071588
0.0 0.0
-1.0 0.0
0.07641363430407891 0.07941349089728894
2163.3266196708328 781.7745024451197
358.79653380609733 462.83187024948637
0.5530856910712836 0.07076380280151584

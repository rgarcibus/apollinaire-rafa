__author__ = 'Sylvain N. Breton'
__uri__ = 'https://apollinaire.readthedocs.io'
__license__ = 'CeCILL'
__version__ = '0.17'
__description__ = 'Module for helio- and asteroseismic data analysis'
__email__ = 'sylvain.breton@cea.fr'

import apollinaire.util

import apollinaire.songlib

import apollinaire.psd

import apollinaire.processing

import apollinaire.simulate

import apollinaire.peakbagging

import apollinaire.timeseries

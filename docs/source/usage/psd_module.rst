The psd module
**************

.. autofunction:: apollinaire.psd.series_to_psd 

.. autofunction:: apollinaire.psd.echelle_diagram 

.. autofunction:: apollinaire.psd.tf 

.. autofunction:: apollinaire.psd.tf_to_psd 

.. autofunction:: apollinaire.psd.plot_psd_fits 
